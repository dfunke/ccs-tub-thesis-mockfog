import fs from 'fs';
import chalk from 'chalk';
import Listr from 'listr';
import { exit } from 'process';

const { execSync } = require('child_process');
const xmljs = require('xml-js');
const util = require('util')


const regexMachineNumber = /\b-\d+$\b/;

const graphmlFileExtracted = 'mockfog-infrastructure';
const graphmlFileToExtract = 'mockfog-infrastructure.graphmlz';
const graphmlKeysNode = 'node';
const graphmlDescription = 'description';
const graphmlNodeLabel = 'NodeLabels';
const graphmlNodeGeometry = 'NodeGeometry';

const maxLatencyDelay = 25;

function getConfig(args) {
  let configFile = fs.readFileSync(args.config, 'utf8');
  let config = JSON.parse(configFile);

  for (const [key, value] of Object.entries(args)) {
    config[key] = value;
  }
                                                                       
  return config;
}

async function extractGraphmlFile(config) {

  try {
    fs.copyFileSync(config.graphz, graphmlFileToExtract);
  } catch (error) {
    console.log(`${chalk.red.bold('ERROR')} Could not copy the compressed graph file from: ${chalk.yellow(config.graphz)}.`);
    throw error;
  }

  try {
    execSync(`7z e \"${graphmlFileToExtract}\" -y`)
  } catch (error) {
    console.log(`${chalk.red.bold('ERROR')} Could not extract the graph file from ${chalk.yellow(graphmlFileToExtract)}.`);
    throw error;
  }
 }

 function getGraphmlNodeKeys(jsonData) {
   let nodeKeys = {};
   for (let graphmlKey of jsonData.graphml.key) {
     if (graphmlKey['_attributes']['for'] == graphmlKeysNode) {
      nodeKeys[graphmlKey['_attributes']['attr.name']] = graphmlKey['_attributes']['id'];
     }
   }
   return nodeKeys;
 }

 function getNode(graphmlNode, nodeKeys) {
  let node = {};
  node['id'] = graphmlNode._attributes.id;

  for (let data of graphmlNode.data) {
    if (data._attributes.key == nodeKeys[graphmlDescription]) {
      node['description'] = JSON.parse(data['_cdata']);
    }
    if (data._attributes.key == nodeKeys[graphmlNodeLabel]) {
      node['label'] = data['x:List']['y:Label']['y:Label.Text']['_text'];
    }
    if (data._attributes.key == nodeKeys[graphmlNodeGeometry]) {
      node['x'] = data['y:RectD']['_attributes']['X'];
      node['y'] = data['y:RectD']['_attributes']['Y'];
    }
  }

  return node;
}

function calculateDistance(a, b) {
  return Math.sqrt(
    Math.pow(a.x - b.x, 2) + 
    Math.pow(a.y - b.y, 2));
}

function parseGraph(config) {
  let xmlFile;
  if (config.extract) {
    xmlFile = fs.readFileSync(graphmlFileExtracted, 'utf8');
  } else {
    xmlFile = fs.readFileSync(config.graphml, 'utf8');
  }
  
  const jsonData = JSON.parse(xmljs.xml2json(xmlFile, {compact: true, spaces: 2}));
  let graph = jsonData.graphml.graph;
  let nodeKeys = getGraphmlNodeKeys(jsonData);

  let machines = {};
  let sidecars = [];
  for (let graphmlNode of graph.node) {
    let node = getNode(graphmlNode, nodeKeys);
    machines[node.id] = node;

    if (node.description && node.description.sidecar) {
      sidecars.push(node.label);
    }
  }

  let connections = [];
  let maxDistance = 0;
  if (isIterable(graph.edge)) {
    for (let edge of graph.edge) {
      let connection = {};
      let source = machines[edge._attributes.source];
      let target = machines[edge._attributes.target];
  
      connection['from'] = source.label;
      connection['to'] = target.label;
    
      connection['distance'] = calculateDistance(source, target);
      connections.push(connection);
  
      if (connection['distance'] > maxDistance) { maxDistance = connection['distance']; }
    }
  }

  return {'graph': graph, 'nodeKeys': nodeKeys, 'machines': machines, 'connections': connections, 'maxDistance': maxDistance, 'sidecars': sidecars};
}

function generateInfrastructure(config, graphml) {
  let infrastructure = {};
  infrastructure['aws'] = {"ec2_region": config.ec2_region , "ssh_key_name": config.ssh_key_name, "agent_port": config.agent_port};

  let machines = [];
  for (let [, value] of Object.entries(graphml.machines)) {
    let machine = {};
    machine['machine_name'] = value.label;
    machine['type'] = config.machine_type;
    machine['image'] = config.machine_image;
    machines.push(machine);
  }

  machines = machines.sort((a, b) => {
    if (a.machine_name < b.machine_name) { return -1; }
    if (a.machine_name > b.machine_name) { return 1; }
    return 0;
  });

  infrastructure['machines'] = machines;
  infrastructure['connections'] = graphml.connections;

  return infrastructure;
}

function calculateDelay(distance, maxDistance) {
  return Math.floor(distance * maxLatencyDelay / maxDistance);
}

function generateConnectionsDelays(infrastructure, graphml) {
  let connections = infrastructure.connections;

  for (let connection of connections) {
    connection['delay'] = calculateDelay(connection['distance'], graphml.maxDistance);
    delete connection['distance'];
  }

  connections = connections.sort((a, b) => {
    if (a.source == b.source) {
      if (a.target < b.target) { return -1; }
      if (a.target > b.target) { return 1; }
    }
    if (a.source < b.source) { return -1; }
    if (a.source > b.source) { return 1; }
    return 0;
  });

  infrastructure['connections'] = connections;
}

function writeJSONFile(data, filename) {
  try {
    fs.writeFileSync(filename, JSON.stringify(data, null, 2));
  } catch (err) {
    console.error(`${chalk.red.bold('ERROR')} Something went wrong during ${filename} file writing.`);
    throw err;
  }
}

function addContainer(containers, containerName, machineName) {
  if (!containers[containerName]) {
    containers[containerName] = [machineName];
  } else {
    containers[containerName].push(machineName);
  }
}

function getSidecarContainer(nodeLabel) {
  let serviceName = nodeLabel.replace(regexMachineNumber, '');
  return nodeLabel.replace(serviceName, serviceName + '-sidecar');
}

function generateDeployment(infrastructure, graphml, config) {
  let containers = {};
  for (let machine of infrastructure.machines) {
    addContainer(containers, machine.machine_name, machine.machine_name);
  }

  for (let machineName of graphml.sidecars) {
    let sidecarContainerName = getSidecarContainer(machineName);
    addContainer(containers, sidecarContainerName, machineName);
  }

  let deployments = [];
  for (let [key, value] of Object.entries(containers)) {
    let deployment = {'container_name': key, 'machine_names': value};

    if (config.machine_resource_percentage) {
      deployment['machine_resource_percentage'] = config.machine_resource_percentage;
    }

    deployments.push(deployment);
  }

  return deployments;
}

function generateContainer(deployments, config) {
  let containers = [];
  for (let deployment of deployments) {
    let container = {};
    let container_template_name = deployment['container_name'].replace(regexMachineNumber, '')
    container = JSON.parse(JSON.stringify(config.container_templates[container_template_name]));
    container['container_name'] = deployment['container_name'];
    container['container_dirname'] = container['container_dirname'].replace(container_template_name, deployment['container_name']);
    container['local_dirname'] = container['local_dirname'].replace(container_template_name, deployment['container_name']);
    container['env']['MACHINE_NAME'] = deployment['container_name'];
    container['env']['ZIPKIN_IP'] = config['zipkin_ip'];
    container['env']['FOGREUR_FRD'] = config['fogreur_frd'];
    containers.push(container);
  }

  return containers;
}

function generateEmptyOrchestration() {
  return { 'states': [] }
}

function writeCoordinatesFile(graphml, coordinatesFile) {
  let data = '';

  for (let [, value] of Object.entries(graphml.machines)) {
    if (value.description && value.description.sidecar) {
      data += `${value.label} ${value.x} ${value.y}\n`
    }
  }

  try {
    fs.writeFileSync(coordinatesFile, data);
  } catch (err) {
    console.error(`${chalk.red.bold('ERROR')} Something went wrong during deployment file writing.`);
    throw err;
  }
}

function fileUnlink(file) {
  fs.unlink(file, (err) => {
    if (err) {
      console.error(err)
    }
  });
}

async function cleanUp(config) {
  if (config.extract) {
    fileUnlink(graphmlFileExtracted);
    fileUnlink(graphmlFileToExtract);
  } else {
    fileUnlink(config.graphml);
  }
}

export async function run(args) {
  let config;
  let graphml;
  let infrastructure;
  let deployments;
  let containers;
  let orchestration;

  const init = new Listr([
    {
      title: `Read configuration file ${chalk.yellow(args.config)}`,
      task: () => {
        try {
          config = getConfig(args)
        } catch (error) {
          console.log(`${chalk.red.bold('ERROR')} Could not read the configuration file ${chalk.yellow(args.config)}. Make sure the configuration file exists and is accessible.`);
          exit()
        }
      }
    }
  ])

  await init.run();
  
  const tasks = new Listr([
    {
      title: `Extract graph file ${chalk.yellow(config.graphz)}`,
      task: () => extractGraphmlFile(config),
      enabled: () => config.extract
    },
    {
      title: `Parse graph file ${(config.extract) ? chalk.yellow(graphmlFileExtracted) : chalk.yellow(config.graphml)}`,
      task: () => {
        try {
          graphml = parseGraph(config)
        } catch (error) {
          console.log(`${chalk.red.bold('ERROR')} Could not parse graph file ${(config.extract) ? chalk.yellow(graphmlFileExtracted) : chalk.yellow(config.graphml)}. Make sure the configuration file exists and is accessible.`);
          exit()
        }
      }
    },
    {
      title: 'Generate infrastructure',
      task: () => infrastructure = generateInfrastructure(config, graphml)
    },
    {
      title: 'Generate delays between machines',
      task: () => generateConnectionsDelays(infrastructure, graphml)
    },
    {
      title: `Write infrastructure file ${chalk.yellow(config.infrastructure_filename)}`,
      task: () => writeJSONFile(infrastructure, config.infrastructure_filename)
    },
    {
      title: 'Generate deployment',
      task: () => deployments = generateDeployment(infrastructure, graphml, config)
    },
    {
      title: `Write deployment file ${chalk.yellow(config.deployment_filename)}`,
      task: () => writeJSONFile(deployments, config.deployment_filename)
    },
    {
      title: 'Generate container info',
      task: () => containers = generateContainer(deployments, config)
    },
    {
      title: `Write container file ${chalk.yellow(config.container_filename)}`,
      task: () => writeJSONFile(containers, config.container_filename)
    },
    
    {
      title: 'Generate orchestration',
      task: () => orchestration = generateEmptyOrchestration()
    },
    {
      title: `Write orchestration file ${chalk.yellow(config.orchestration_filename)}`,
      task: () => writeJSONFile(orchestration, config.orchestration_filename)
    },
    {
      title: `Write Coordinates CSV file ${chalk.yellow("coordinates.csv")}`,
      task: () => writeCoordinatesFile(graphml, "coordinates.csv"),
      enabled: () => config.coordinates
    },
    {
      title: 'Clean up',
      task: () => cleanUp(config),
      enabled: () => config.cleanup
    }
  ]);

  await tasks.run();
}

function isIterable (value) {
  return Symbol.iterator in Object(value);
}